export default theme => ({
  LandingMainContent: {
    //height: '92vh',
    //background: `${theme.palette.primary.main}`
  },
  logo: {
    width: 72,
    height: 72,
  },
  createBtn: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2),
  },
});
