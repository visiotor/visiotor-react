import React from 'react';

const UnderMaintainance = () => {
  return (
  <article>
    <h1>We&rsquo;ll be back soon!</h1>
    <div>
      <p>Sorry for the inconvenience but we&rsquo;re performing some maintenance at the moment. If you need to you can always <a href="mailto:#">contact us</a>, otherwise we&rsquo;ll be back online shortly!</p>
      <p>&mdash; The Visiotor.com Team</p>
    </div>
  </article>
  );
};

export default UnderMaintainance;
