import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import Divider from '@material-ui/core/Divider';
class DataProtection extends Component {
  render() {
    return (
      <Grid container justify={"center"} alignContent={"center"}>
        <Grid item xs={9}>
          <Typography variant={"h5"} align={"left"}>Angaben gemäß § 5 TMG</Typography>
          <Typography variant={'subtitle1'}>
            Khalil Boroumandi Shirazi<br/>
            Horststr.13<br/>
            47137 Duisburg<br/>
          </Typography>
          <Typography variant={'subtitle2'}>
            Kontakt
          </Typography>
          <Typography variant={'caption'}>
            Telefon: 01631789102 <br/>
            E-Mail: info@visiotor.com <br/>
          </Typography>
          <Typography variant={'subtitle2'}>
            Umsatzsteuer-ID
          </Typography>
          <Typography variant={'caption'}>
            Umsatzsteuer-Identifikationsnummer gemäß §27 a Umsatzsteuergesetz: <br/>
            83670495827 <br/>
            Wir sind nicht bereit oder verpflichtet, an Streitbeilegungsverfahren vor einer <br/>
            Verbraucherschlichtungsstelle teilzunehmen. <br/>
          </Typography>
          <Typography variant={'subtitle2'}>
            Haftung für Inhalte
          </Typography>
          <Typography variant={'caption'}>
            Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den
            allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht
            verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach
            Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.<br/>
            Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen
            Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
            Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden
            Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
          </Typography>
          <Typography variant={'subtitle2'}>
            Haftung für Links
          </Typography>
          <Typography variant={'caption'}>
            Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss
            haben. <br/>
            Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der
            verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die
            verlinkten
            Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige
            Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. <br/>
            Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte
            einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir
            derartige Links umgehend entfernen.
          </Typography>

          <Typography variant={'subtitle2'}>
            Urheberrecht
          </Typography>
          <Typography variant={'caption'}>
            Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem
            deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung
            außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen
            Autors bzw. Erstellers. <br/>
            Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.
            Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte
            Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem
            auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis.
            Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
            Quelle: <a href="http://e-recht24.de">e-recht24.de</a>
          </Typography>

        </Grid>
      </Grid>
    );
  }
}


export default DataProtection;
