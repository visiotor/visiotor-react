import React, { Component } from 'react';
import { Grid, withStyles } from '@material-ui/core';
import styles from './styles';
import VisionCard from '../../components/vision-card';
import VisionLoader from '../../components/vision-loader';
import DetailDialog from '../../components/detail-dialog';
import { getMyVisions, getAllVisions } from '../../utils/api';
import Button from '@material-ui/core/Button';
import CreateVisionDialog from '../../components/create-vision-dialog';

class LandingPage extends Component {
  state = {
    fetching: true,
    showDetail: false,
    auth: !!localStorage.token,
    visions: [],
    openCreateVision: false,
  };

  componentDidMount() {
    console.log(this.props.match.params.category);
    this.fetch();
  }

  handleCloseDetail = () => {
    this.setState({
      showDetail: false,
    });
  };

  fetch = async () => {
    try {
      this.setState({
        fetching: true,
      });
      const visionsRes = await getAllVisions();
      if (visionsRes.success) {
        this.setState({
          visions: visionsRes.payload,
        });
      }
    } catch (e) {
      console.log(e);
    } finally {
      this.setState({
        fetching: false,
      });
    }
  };
  handleCreateVision = () => {
    console.log('hello');
    this.setState({
      openCreateVision: true,
    });
  };

  handleCloseCreateVisonDialog = () => {
    this.setState({
      openCreateVision: false,
    });
  };

  handleCreateVisionResult = (error) => {
    if (error) {
      this.handleCloseCreateVisonDialog();
      //setMessage(error.message);
      //setError(true);
    } else {
      this.handleCloseCreateVisonDialog();
      this.fetch();
    }
  };

  componentWillReceiveProps(nextProps, nextContext) {
  }

  render() {
    const { classes } = this.props;
    return this.state.fetching ? (
      <Grid xs={12} md={9} item={true} alignItems={'center'}>
        <VisionLoader/>
      </Grid>
    ) : (
      <div>
        {localStorage.token?  (<Button className={classes.createBtn} fullWidth variant={'outlined'} color={'primary'}
                                       onClick={this.handleCreateVision}>Create A Vision</Button>) : null}

        {this.state.visions.map(i => (
        <Grid xs={12} className={classes.LandingMainContent} alignItems={'center'} key={i}>
          <VisionCard
            title={i.title}
            description={i.description}
            image={i.coverImage}
            {...i}
            refresh={this.fetch}
            onClick={id => {
              this.setState(
                {
                  showDetail: true,
                  id,
                },
                () => this.dialogRef.fetch(),
              );
            }}
          />
          <DetailDialog
            open={this.state.showDetail}
            handleClose={this.handleCloseDetail}
            id={this.state.id}
            ref={ref => (this.dialogRef = ref)}
          />

        </Grid>

        ))}
        <CreateVisionDialog
          open={this.state.openCreateVision}
          onClose={this.handleCloseCreateVisonDialog}
          createVisionSuccess={this.handleCreateVisionResult}
          createVisionFailed={this.handleCreateVisionResult}
        />
      </div>
    );
  }
}

export default withStyles(styles)(LandingPage);
