import React, { Component } from 'react';
import {
  Grid,
  Typography,
  LinearProgress,
  withStyles,
  Container,
  Paper,
  CardHeader,
} from '@material-ui/core';
import { Share, Favorite, Report } from '@material-ui/icons';

import { getVisionDetail } from '../../utils/api';
import styles from './styles';
import Button from '@material-ui/core/Button';
import { SERVER_IP_OR_ADDRESS, API_VERSION } from '../../utils/api';

class VisionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fetching: false,
    };
  }

  setFetching = () => this.setState({ fetching: true });
  cancelFetching = () => this.setState({ fetching: false });

  componentDidMount() {
    const { id } = this.props.match.params;
    if (id) {
      this.setFetching();
      this.fetchDetail(id);
    } else {
      this.props.history.push('/');
    }
  }

  fetchDetail = async id => {
    const { success, payload } = await getVisionDetail(id);
    if (success) {
      this.setState({
        fetching: false,
        ...payload,
      });
    }
  };

  render() {
    const { fetching, owner, _id, title, description, coverImage, mission, requirements } = this.state;
    const { classes } = this.props;
    return (
      <Container>
        {fetching ? <LinearProgress variant={'indeterminate'} color={'secondary'}/> : null}
        <Grid item xs={12}>
          <Paper>
            <CardHeader title={owner && owner.fullname} subheader={owner && `@${owner.username}`}/>
            {/*<CardActions >
              <Button>Follow User</Button>
            </CardActions>*/}
          </Paper>

          <Paper className={classes.card}>
            <Typography variant={'h5'}>{title}</Typography>
            <Typography variant={'caption'}>{description}</Typography>
            <img className={classes.coverImage} alt={coverImage && coverImage.filename}
                 src={`${SERVER_IP_OR_ADDRESS}/${API_VERSION}/vision/${_id}/cover`}/>
          </Paper>
          <Paper className={classes.card}>
            <Typography variant={'h5'}>Our Mission</Typography>
            <Typography variant={'caption'}>{mission}</Typography>
            <Typography variant={'h5'}>We Need</Typography>
            <Typography variant={'caption'}>{requirements}</Typography>
          </Paper>
          <Paper className={classes.card}>
            <Grid container>
              <Grid item xs={4}>
                <Button fullWidth>
                  <Favorite/>
                  Like
                </Button>
              </Grid>
              <Grid item xs={4}>
                <Button fullWidth>
                  <Share/>
                  Share
                </Button>
              </Grid>
              <Grid item xs={4}>
                <Button fullWidth>
                  <Report/>
                  Report
                </Button>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Container>
    );
  }
}

export default withStyles(styles)(VisionDetail);
