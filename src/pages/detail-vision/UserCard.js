import React from 'react';
import PropTypes from 'prop-types';
import { Paper, withStyles, Button, Avatar, Typography } from '@material-ui/core';

UserCard.propTypes = {
  fullname: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  isYou: PropTypes.bool.isRequired,
  followed: PropTypes.bool.isRequired,
};

const styles = theme => ({
  paper: {
    marginTop: theme.spacing(1),
  },
});

function UserCard({ classes, id, fullname, type, email, isYou, followed }) {
  return (
    <Paper className={classes.paper} elevation={8}>

  </Paper>
  );
}

export default withStyles(styles)(UserCard);
