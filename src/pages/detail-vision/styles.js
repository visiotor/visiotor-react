export default theme => ({
  progress: {
    position: 'absolute',
    top: 0,
  },
  card: {
    marginTop: theme.spacing(1),
    padding: theme.spacing(1)
  },
  coverImage: {
    width: '100%',
    maxHeight: 360
  }
});
