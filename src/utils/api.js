/**
 * Created by milad on 1/27/18.
 */
import fetch from './fetchWithTimeout';
import axios from 'axios';

export const SERVER_IP_OR_ADDRESS = 'http://api.visiotor.com';
export const API_VERSION = 'v1';

const getUrl = route => `${SERVER_IP_OR_ADDRESS}/${API_VERSION}/${route}`;

const getHeaders = (withAuth, withFile) => ({
  Authorization: withAuth ? `Bearer ${localStorage['token']}` : undefined,
  'Content-Type': withFile ? 'application/x-www-form-urlencoded' : 'application/json',
});

export const login = (username, password) =>
  fetch(getUrl('user/login'), {
    headers: getHeaders(false),
    method: 'POST',
    body: JSON.stringify({
      username,
      password,
    }),
  }).then(res => res.json());

export const register = (username, fullname, country, city, location, password, email) =>
  fetch(getUrl('user/register'), {
    method: 'POST',
    headers: getHeaders(false),
    body: JSON.stringify({
      username,
      email,
      fullname,
      location,
      country,
      city,
      password,
    }),
  }).then(res => res.json());

export const getMyVisions = () =>
  fetch(getUrl('vision/my'), {
    method: 'GET',
    headers: getHeaders(true),
  }).then(res => res.json());

export const getAllVisions = () =>
  fetch(getUrl('vision/'), {
    method: 'GET',
    headers: getHeaders(true),
  }).then(res => res.json());

export const createVision = (
  title,
  description,
  mission,
  requirements,
  coverImage,
  gallery,
  videos,
  category,
  summary,
) => {
  const formData = new FormData();
  formData.append('title', title);
  formData.append('description', description);
  formData.append('mission', mission);
  formData.append('summary', summary);
  formData.append('requirements', requirements);
  formData.append('coverImage', coverImage);
  formData.append('gallery', gallery);
  formData.append('video', videos);
  category.map((c, index) => {
    formData.append(`category.${index}`, c);
    return null;
  });

  return axios.post(getUrl('vision'), formData, {
    headers: {
      Authorization: localStorage.token ? `Bearer ${localStorage['token']}` : undefined,
    },
  });
  // return fetch(
  //   getUrl('vision'), {
  //     method: 'POST',
  //     headers: getHeaders(true, true),
  //     body: formData,
  //   },
  // ).then(res => res.json());
};

export const getVisionByCategory = category =>
  fetch(getUrl(`vision/${category}/filterByCategory`), {
    method: 'GET',
    headers: getHeaders(false),
  }).then(res => res.json());

export const getVisionDetail = id =>
  fetch(getUrl(`vision/${id}`), {
    method: 'GET',
    headers: getHeaders(false),
  }).then(res => res.json());

export const getLikedStatus = visionId =>
  axios.get(getUrl(`vision/${visionId}/liked`), {
    headers: {
      Authorization: localStorage.token ? `Bearer ${localStorage['token']}` : undefined,
    },
  });

export const deleteVision = visionId =>
  axios.delete(getUrl(`vision/${visionId}`), {
    headers: {
      Authorization: localStorage.token ? `Bearer ${localStorage['token']}` : undefined,
    },
  });

export const setLike = visionId =>
  axios.put(
    getUrl(`vision/${visionId}/like`),
    {},
    {
      headers: {
        Authorization: localStorage.token ? `Bearer ${localStorage['token']}` : undefined,
      },
    },
  );

export const deleteLike = visionId =>
  axios.delete(getUrl(`vision/${visionId}/like`), {
    headers: {
      Authorization: localStorage.token ? `Bearer ${localStorage['token']}` : undefined,
    },
  });
