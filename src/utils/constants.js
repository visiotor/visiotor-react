export const FETCH_TIME_OUT_MILLIS = 40000

export const categories = ['Holistic', 'Community', 'Self Recognition', 'Health', 'Food', 'Education', 'Believing', 'Practice', 'Live'
    , 'Build', 'Energy', 'Mobility', 'Culture & Art', 'Digital World', 'Ecology', 'Economy', 'Politics', 'Rights', 'Communication']
