import {FETCH_TIME_OUT_MILLIS} from "./constants";

export default function (url, options, timeout = FETCH_TIME_OUT_MILLIS) {
    return Promise.race([
        fetch(url, options),
        new Promise((_, reject) =>
            setTimeout(() => reject(new Error('timeout')), timeout)
        )
    ]).catch(error => {
        console.log(error.message)
    });
}
