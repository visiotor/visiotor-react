import React from 'react';
import { Switch, Route, withRouter, Link } from 'react-router-dom';
import {
  AppBar,
  Grid,
  Toolbar,
  Typography,
  Button,
  makeStyles,
  IconButton,
} from '@material-ui/core';
import { Home as HomeIcon } from '@material-ui/icons';
//import logo from './images/logo.png';
import LandingPage from './pages/home-page';
import FilterByCategoryPage from './pages/filter-by-category';
import './App.css';
import { fade } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import LoginOrRegisterDialog from './components/login-or-register-dialog';
import Snackbar from '@material-ui/core/Snackbar';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import CreateVisionDialog from './components/create-vision-dialog';
import { categories } from './utils/constants';
import VisionDetail from './pages/detail-vision';
import UnderMaintainance from './pages/under';
import DataProtection from './pages/data-protection';
import Imprint from './pages/imprint';
import Footer from './components/footer';

const useStyle = makeStyles(theme => ({
  title: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  list: {
    width: 250,
  },
  fullList: {
    width: 'auto',
  },
  logo: {
    height: 200,
    width: 250,
    position: 'absolute',
    bottom: 0,
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.black, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.black, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
  progress: {
    position: 'absolute',
    top: 0,
  },
  content: {
    marginBottom: 50,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    flexGrow: 1,
    flex: 1,
    justifyContent: 'center',
  },
  footerContainer: {
    height: 50,
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    position: 'fixed',
    bottom: 0,
  },
  oneFlex: {
    flex: 1,
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column',
  },
  twoFlex: {
    flex: 2,
    display: 'flex',
    flexGrow: 2,
    flexDirection: 'column',
  },
  threeFlex: {},
  visionList: {
    flex: 3,
    display: 'flex',
    flexGrow: 3,
    flexDirection: 'column',
    marginLeft: 230,
  },
  logo: {
    width: 230,
    position: 'fixed',
    bottom: 0,
  },
  worldSideBar: {
    position: 'fixed',
    left: 0,
    width: 230,
  },
  worldItem: {
    margin: 24,
  },
  categoryItem: {
    textDecoration: 'none',
    margin: 8,
    width: '100%',
    textAlign: 'left',
  },
  categortItemActive: {
    textDecoration: 'none',
    margin: 8,
    padding: 4,
    width: '100%',
    textAlign: 'left',
    backgroundColor: theme.palette.primary.main,
    borderRadius: 4,
  },
  profileSideBar: {
    flex: 2,
    flexGrow: 2,
    justifyContent: 'center',
    alignContent: 'center',
    flexDirection: 'column',
    padding: 32,
  },
}));

function App(props) {
  const classes = useStyle();
  const [state, setState] = React.useState({
    drawerOpen: false,
    openLoginDialog: false,
    openCreateVision: false,
  });

  const homeRef = React.useRef();

  const [error, setError] = React.useState(false);
  const [message, setMessage] = React.useState('');
  const [auth, setAuth] = React.useState(!!localStorage.token);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  function handleCloseLoginDialog() {
    setState({
      openLoginDialog: false,
    });
  }

  function handleOpenLoginDialog() {
    setState({
      openLoginDialog: true,
    });
  }

  function handleMenu(event) {
    setAnchorEl(event.currentTarget);
  }

  function handleCreateVision() {
    handleClose();
    setState({
      openCreateVision: true,
    });
  }

  function handleCloseCreateVisonDialog() {
    setState({
      openCreateVision: false,
    });
  }

  function handleClose() {
    setAnchorEl(null);
  }

  function handleLogout() {
    handleClose();
    setAuth(false);
    localStorage.clear();
  }

  function handleLogin() {
    handleCloseLoginDialog();
    if (localStorage.token) {
      setAuth(true);
      homeRef.current.fetch();
    } else {
      setError(true);
      setMessage('Username or Password Incorrect...');
    }
  }

  function handleRegFailed(data) {
    handleCloseLoginDialog();
    setMessage(data.message);
    setError(true);
  }

  function handleRegSuccess() {
    handleCloseLoginDialog();
    setMessage('Register Successful');
    setError(true);
  }

  function handleCreateVisionResult(error) {
    if (error) {
      handleCloseCreateVisonDialog();
      setMessage(error.message);
      setError(true);
    } else {
      handleCloseCreateVisonDialog();
      homeRef.current.fetch();
    }
  }

  return (
    <Grid
      container
      justify={'center'}
      alignItems={'center'}
      alignContent={'center'}
      direction={'column'}
    >
      <AppBar position="sticky" color="primary">
        <Toolbar>
          <Link className={classes.menuButton} to="/">
            <IconButton edge="start" color="inherit" aria-label="Menu">
              <HomeIcon />
            </IconButton>
          </Link>
          <Typography variant="h6" color="inherit" align={'center'} className={classes.title}>
            Visiotor
          </Typography>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
            />
          </div>
          {auth ? (
            <div>
              <IconButton
                aria-label="Account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="menu-appbar"
                anchorEl={anchorEl}
                anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                  vertical: 'top',
                  horizontal: 'right',
                }}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
              </Menu>
            </div>
          ) : (
            <Button color="inherit" onClick={handleOpenLoginDialog}>
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>

      <div className={classes.content}>
        <div id="world-sidebar" className={classes.worldSideBar}>
          <Typography variant="h5" align="center">
            The World
          </Typography>
          <Typography align="left" className={classes.worldItem}>
            Visions
          </Typography>
          <Typography align="left" className={classes.worldItem}>
            Networks
          </Typography>
          <Typography align="left" className={classes.worldItem}>
            News
          </Typography>
          <img className={classes.logo} src={'/images/logo.png'} alt="visiotor-logo" />
        </div>
        <div id="content" className={classes.visionList}>
          <Switch>
            <Route path={'/'} exact render={props => <LandingPage ref={homeRef} {...props} />} />
            <Route path={'/_v/:id'} render={props => <VisionDetail {...props} />} />
            <Route
              path={'/_c/:category'}
              render={props => (
                <FilterByCategoryPage
                  {...props}
                  key={props.location.pathname + props.location.search}
                />
              )}
            />
            <Route path={'/data-protection'} render={props => <DataProtection {...props} />} />
            <Route path={'/imprint'} render={props => <Imprint {...props} />} />
            <Route path={'/suggestions'} render={() => <UnderMaintainance />} />
            <Route render={() => <UnderMaintainance />} />
          </Switch>
        </div>
        <div id="categories" className={classes.oneFlex}>
          <Typography variant="h5" align="center">
            Explore more...
          </Typography>
          {categories.map(category => (
            <Link
              className={
                props.history.location.pathname === `/_c/${category}`
                  ? classes.categortItemActive
                  : classes.categoryItem
              }
              to={`/_c/${category}`}
            >
              {category}
            </Link>
          ))}
        </div>
        <div className={classes.profileSideBar}>
          {localStorage['token'] ? (
            <React.Fragment>
              <Link>
                <Typography className={classes.categoryItem}>My Visions</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>Followed Visions</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>My Organisations</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>My Posts</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>Networks</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>News</Typography>
              </Link>
              <Link>
                <Typography className={classes.categoryItem}>Firends</Typography>
              </Link>
            </React.Fragment>
          ) : (
            <Typography>Please Login to your account or register</Typography>
          )}
        </div>
      </div>

      <Footer />

      <LoginOrRegisterDialog
        open={state.openLoginDialog}
        onClose={handleCloseLoginDialog}
        loginSuccess={handleLogin}
        loginFailed={handleLogin}
        regSuccess={handleRegSuccess}
        regFailed={handleRegFailed}
      />
      <CreateVisionDialog
        open={state.openCreateVision}
        onClose={handleCloseCreateVisonDialog}
        createVisionSuccess={handleCreateVisionResult}
        createVisionFailed={handleCreateVisionResult}
      />
      <Snackbar
        open={error}
        message={message}
        autoHideDuration={4000}
        onClose={() => setError(false)}
      />
    </Grid>
  );
}

export default withRouter(App);
