import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import LoadingButton from "../loading-button";
import {register} from "../../utils/api";
import {Paper} from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";
import {Map, Marker, TileLayer} from "react-leaflet";
import Typography from "@material-ui/core/Typography";

class RegisterForm extends Component {

    state = {
        fetching: false,

    };

    handleSubmit = async () => {
        this.setState({
            fetching: true
        });
        try {
            const registerRes = await register(
                this.usernameRef.value, this.fullnameRef.value,
                this.countryRef.value, this.cityRef.value,
                this.state.locationPicked,
                this.passwordRef.value, this.emailRef.value);
            if (registerRes.success) {
                this.props.regSuccess();
            } else {
                this.props.regFailed(registerRes.payload)
            }
        } catch (e) {
            console.log(e);
        } finally {
            this.setState({
                fetching: false
            })
        }
    };
    pickLocation = event => {
        this.setState({
            locationPicked: event.latlng
        })
    }

    render() {
        return (
            <Paper elevation={4} className={this.props.classes.formPaper}>
                <TextField
                    autoFocus
                    margin="dense"
                    id="register-email"
                    label="Email Address"
                    type="email"
                    inputRef={ref => this.emailRef = ref}
                    required
                    fullWidth
                />

                <TextField
                    margin="dense"
                    id="register-username"
                    label="user name"
                    required
                    fullWidth
                    inputRef={ref => this.usernameRef = ref}
                    helperText={"Unique identifier"}
                />

                <TextField
                    margin="dense"
                    id="register-fullname"
                    label="Full Name"
                    required
                    inputRef={ref => this.fullnameRef = ref}
                    fullWidth
                />

                <TextField
                    margin="dense"
                    id="register-country"
                    label="Country"
                    required
                    inputRef={ref => this.countryRef = ref}
                    fullWidth
                />

                <TextField
                    margin="dense"
                    id="register-city"
                    label="City"
                    required
                    inputRef={ref => this.cityRef = ref}
                    fullWidth
                />

                <TextField
                    margin="dense"
                    id="register-password"
                    label="Password"
                    type="password"
                    inputRef={ref => this.passwordRef = ref}
                    required
                    fullWidth
                />
                <Typography color={"textPrimary"} variant={"subtitle2"}>
                    Pick you region location
                </Typography>
                <Map
                    style={{
                        width: '100%',
                        height: '40vh'
                    }}
                    onClick={this.pickLocation}
                    center={[29.6163944283006, 52.5184625387192]} scrollWheelZoom={false} zoom={1}>
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        attribution="&copy;"
                    />
                    {this.state.locationPicked ? (
                        <Marker position={[this.state.locationPicked ? this.state.locationPicked.lat : null, this.state.locationPicked ?  this.state.locationPicked.lng: null]}/>
                    ) : null}
                </Map>
                <LoadingButton fetching={this.state.fetching} fullWidth
                               onClick={this.handleSubmit}>Register</LoadingButton>
            </Paper>
        );
    }
}

export default withStyles({
    formPaper: {
        padding: 8,
        margin: 8
    }
})(RegisterForm);
