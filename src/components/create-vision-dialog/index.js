import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { Container } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import LoadingButton from '../loading-button';
import SelectCategory from '../select-category';
import { createVision } from '../../utils/api';
import FilePicker from '../file-picker';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'relative',
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
  },
  formPaper: {
    padding: 8,
    margin: 8,
  },
  submitButton: {
    marginTop: 10,
  },
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function CreateVisionDialog(props) {
  const classes = useStyles();

  const [fetching, setFetching] = React.useState(false);
  const [title, setTitle] = React.useState('');
  const [description, setDescription] = React.useState('');
  const [summary, setSummary] = React.useState('');
  const [requirements, setRequirements] = React.useState('');
  const [mission, setMission] = React.useState('');
  const [video, setVideo] = React.useState('');
  const [coverImage, setCoverImage] = React.useState('');
  const [gallery, setGallery] = React.useState('');

  const [categories, setCategories] = React.useState([]);

  async function handleSubmit() {
    setFetching(true);
    try {
      const createRes = await createVision(title, description, mission, requirements, coverImage, gallery, video, categories, summary);
      if (createRes.success) {
        props.createVisionSuccess();
      } else {
        props.createVisionFailed(createRes.payload);
      }
    } catch (e) {
      console.log(e);
    } finally {
      setFetching(false);
    }
  }

  function handleCoverImage(image) {
      console.log(image);
    setCoverImage(image[0]);
  }

  function handleGalleryImages(images) {
    setGallery(images);
  }

  return (
    <Dialog fullScreen open={props.open} onClose={props.onClose} TransitionComponent={Transition}>
      <AppBar className={classes.appBar}>
        <Toolbar>
          <IconButton edge="start" color="inherit" onClick={props.onClose} aria-label="Close">
            <CloseIcon/>
          </IconButton>
          <Typography variant="h6" className={classes.title} align={'center'}>
            Create Vision
          </Typography>
        </Toolbar>
      </AppBar>

      <Container>
        <Paper className={classes.formPaper}>
          <Typography variant={'subtitle1'}>Cover Image</Typography>
          <FilePicker label={'Drag or click here to choose Cover Image'} multiple={false} onChange={handleCoverImage}/>
          <Typography variant={'subtitle1'}>Gallery Images</Typography>
          <FilePicker label={'Drag or click here to choose Gallery Images. You can choose multiple images'}
                      multiple={true} onChange={handleGalleryImages}/>
          <TextField id={'title-create-vision'} label={'Title'} fullWidth value={title}
                     onChange={event => setTitle(event.target.value)}/>
          <TextField id={'description-create-vision'} multiline rows={4} label={'Description'} fullWidth
                     value={description} onChange={event => setDescription(event.target.value)}/>
          <TextField id={'summary-create-vision'} multiline rows={4} label={'Summary'} fullWidth
                     value={summary} onChange={event => setSummary(event.target.value)}/>
          <TextField id={'mission-create-vision'} multiline rows={4} label={'Mission'} fullWidth
                     value={mission} onChange={event => setMission(event.target.value)}/>
          <TextField id={'req-create-vision'} multiline rows={4} label={'Requirements'} fullWidth
                     value={requirements} onChange={event => setRequirements(event.target.value)}/>
          <SelectCategory onSelect={(vals) => setCategories(vals)}/>
          <TextField id={'video-create-vision'} label={'Video URL'} fullWidth value={video}
                     onChange={event => setVideo(event.target.value)}/>

          <LoadingButton fetching={fetching} onClick={handleSubmit} className={classes.submitButton} fullWidth>
            Create Vision
          </LoadingButton>
        </Paper>

      </Container>


    </Dialog>
  );
}

export default CreateVisionDialog;
