import React, {Component} from 'react';
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Divider from "@material-ui/core/Divider";
import Checkbox from "@material-ui/core/Checkbox";
import LoadingButton from "../loading-button";
import {login} from '../../utils/api';
import Paper from "@material-ui/core/Paper";
import withStyles from "@material-ui/core/styles/withStyles";

class LoginForm extends Component {
    state = {
        submitting : false,
    };

    submitLogin = async () => {
        this.setState({
            submitting: true
        });
        try{
            const loginres = await login(this.usernameRef.value, this.passwordRef.value);
            localStorage.token = loginres.token;
            localStorage.username = loginres.user;
            this.props.loginSuccess();
        }catch (e) {
            this.props.loginFailed();
        }finally {
            this.setState({
                submitting: false
            })
        }
    }
    render() {
        return (
            <Paper elevation={4} className={this.props.classes.formPaper} >
                <TextField
                    autoFocus
                    margin="dense"
                    id="login-username"
                    label="Username"
                    required
                    fullWidth
                    inputRef={ref => this.usernameRef = ref}
                />
                <TextField
                    margin="dense"
                    id="login-password"
                    label="Password"
                    type="password"
                    required
                    fullWidth
                    inputRef={ref => this.passwordRef = ref}
                />

                <FormControlLabel
                    control={
                        <Checkbox
                        />
                    }
                    label="Remember me"
                />
                <Divider />
                <LoadingButton fetching={this.state.submitting} disabled={this.state.submitting} fullWidth onClick={this.submitLogin}>Login</LoadingButton>
            </Paper>
        );
    }
}

export default withStyles({
    formPaper: {
        padding: 8,
        margin : 8
    }
})(LoginForm);
