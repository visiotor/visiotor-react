import React from 'react';
import {Instagram} from "react-content-loader";
import {withTheme} from '@material-ui/core/styles'

export default withTheme((props) => {
        return (
            <Instagram
                style={{
                    height:500,
                    width: 400,
                    marginTop: 100
                }}
                speed={2}
                height={500}
                width={400}
                primaryColor={props.theme.palette.secondary.main}
                secondaryColor={props.theme.palette.primary.main}
                ariaLabel={"Loading..."}
            />
        )
    }
)
