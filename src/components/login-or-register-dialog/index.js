import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {Container} from '@material-ui/core';
import SwipeableViews from 'react-swipeable-views';
import LoginForm from "../login-form";
import RegisterForm from "../register-form";

const useStyles = makeStyles(theme => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
    },
    formPaper: {
        padding: 8,
        margin : 8
    }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function LoginOrRegisterDialog(props) {
    const classes = useStyles();
    const [tabValue, setTabValue] = React.useState(0);


    function handleChangeIndex(event, newVal) {
        setTabValue(newVal)
    }

    return (
        <Dialog fullScreen open={props.open} onClose={props.onClose} TransitionComponent={Transition}>
            <AppBar className={classes.appBar}>
                <Toolbar>
                    <IconButton edge="start" color="inherit" onClick={props.onClose} aria-label="Close">
                        <CloseIcon/>
                    </IconButton>
                    <Typography variant="h6" className={classes.title} align={"center"}>
                        Login / Register here
                    </Typography>
                </Toolbar>
                <Tabs variant={"fullWidth"} value={tabValue} onChange={handleChangeIndex}>
                    <Tab label={'Login'}/>
                    <Tab label={'Register'}/>
                </Tabs>
            </AppBar>

            <Container maxWidth={"md"}>
                    <SwipeableViews
                        index={tabValue}
                        onChangeIndex={handleChangeIndex}>
                        <LoginForm loginSuccess={props.loginSuccess}
                                   loginFailed={props.loginFailed}
                        />
                        <RegisterForm
                            regSuccess={props.regSuccess}
                            regFailed={props.regFailed}
                        />
                    </SwipeableViews>
            </Container>

        </Dialog>
    );
}

export default LoginOrRegisterDialog;
