import React, { useCallback, useState , useEffect} from 'react';
import { withStyles, Container } from '@material-ui/core';
import Dropzone from 'react-dropzone';

const styles = theme => ({
  container: {
    border: '1px dashed black',
    borderRadius: '1px',
    marginTop: theme.spacing(1),
  },
  image: {
    width: 150,
    height: 80,
    margin: theme.spacing(1),
  },
});
const FilePicker = ({ classes, label, multiple, onChange }) => {
  const [images, setImages] = useState([]);

  function handleDrop(acceptedFiles) {
    setImages(acceptedFiles.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file),
    })));

  }


  useEffect(() => {
    onChange(images);
  });

  return (
    <Dropzone onDrop={handleDrop} multiple={multiple} accept={'image/*'}>
      {({ getRootProps, getInputProps }) => (
        <Container className={classes.container}>
          <div {...getRootProps()}>
            <input {...getInputProps()} />
            {images.length > 0 ? (
              images.map((image, index) => (
                <img className={classes.image} key={index} src={image.preview}/>
              ))
            ) : (
              <p>{label}</p>
            )}

          </div>
        </Container>
      )}
    </Dropzone>

  );
};

export default withStyles(styles)(FilePicker);
