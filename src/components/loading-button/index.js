import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';

class LoadingButton extends Component {
    render() {
        return (
            <Button {...this.props}>
                {this.props.fetching ? (
                    <CircularProgress color={"secondary"} size={20}  />
                ) : (
                    this.props.children
                )}
            </Button>
        );
    }
}

export default LoadingButton;
