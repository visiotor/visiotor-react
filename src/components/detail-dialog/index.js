import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { withStyles } from '@material-ui/core';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import { Grid } from '@material-ui/core';
import { getVisionDetail } from '../../utils/api';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';

const styles = theme => ({
  visionCover: {
    width: '100%',
    height: 220,
    marginBottom: 8,
  },
});

class DetailDialog extends React.Component {
  state = {
    fetching: false,
  };

  fetch = async () => {
    this.setState({
      fetching: true,
    });
    try {
      const vision = await getVisionDetail(this.props.id);
      this.setState({
        vision: vision.payload,
        fetching: false,
      });
    } catch (e) {
      this.setState({
        fetching: false,
      });
    }
  };

  render() {
    const { classes } = this.props;
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.handleClose}
          scroll={'body'}
          maxWidth={'md'}
          aria-labelledby="detail-dialog"
        >
          {this.state.fetching ? (
            <Grid container justify={'center'}>
              <CircularProgress
                style={{
                  margin: 32,
                }}
                variant={'indeterminate'}
                size={60}
                color={'secondary'}
              />
            </Grid>
          ) : this.state.vision ? (
            <React.Fragment>
              <DialogTitle id="scroll-dialog-title">{this.state.vision.title}</DialogTitle>
              <DialogContent>
                <img
                  src={this.state.vision.images[0]}
                  alt={`cover-${this.props.id}`}
                  className={classes.visionCover}
                />
                <Typography>Summary</Typography>
                <DialogContentText paragraph>{this.state.vision.summary}</DialogContentText>
                <Divider />
                <Typography>Description</Typography>
                <DialogContentText color={'textPrimary'} paragraph>
                  {this.state.vision.description}
                </DialogContentText>
                <Divider />
                <Typography>Mission</Typography>
                <DialogContentText color={'textPrimary'} paragraph>
                  {this.state.vision.mission}
                </DialogContentText>
                <Divider />
                <Typography>Requirements</Typography>
                <DialogContentText color={'textPrimary'} paragraph>
                  {this.state.vision.requirements}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.props.handleClose} variant={'outlined'}>
                  Cancel
                </Button>
              </DialogActions>
            </React.Fragment>
          ) : null}
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(DetailDialog);
