import React from 'react';
import { withRouter, Link } from 'react-router-dom';
import { makeStyles, Typography, Button } from '@material-ui/core';

const useStyle = makeStyles(theme => ({
  footerContainer: {
    height: 50,
    width: '100%',
    backgroundColor: theme.palette.primary.main,
    position: 'fixed',
    bottom: 0,
    flexDirection: 'row',
    display: 'flex',
  },
  item: {
    flexGrow: 1,
    flex: 1,
    height: '100%',
    alignContent: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    textDecoration: 'none',
    color: theme.palette.text.primary,
  },
  text: {
    flexGrow: 1,
    flexDirection: 'column',
    textAlign: 'center',
    justifyContent: 'center',
  },
}));

export default withRouter(({ history }) => {
  const classes = useStyle();
  return (
    <div className={classes.footerContainer}>
      <Button variant="text" className={classes.item}>
        <Link to={'/imprint'}>
          <Typography variant={'subtitle'} className={classes.text}>
            Imprint
          </Typography>
        </Link>
      </Button>
      <Button variant="text" className={classes.item}>
        <Link to={'/data-protection'}>
          <Typography variant={'subtitle'} className={classes.text}>
            Data Protection
          </Typography>
        </Link>
      </Button>
      <Button variant="text" className={classes.item}>
        <Typography variant={'subtitle'} className={classes.text}>
          Language
        </Typography>
      </Button>

      <Button variant="text" className={classes.item}>
        <Typography variant={'subtitle'} className={classes.text}>
          Mission
        </Typography>
      </Button>

      <Button variant="text" className={classes.item}>
        <Typography variant={'subtitle'} className={classes.text}>
          Contact
        </Typography>
      </Button>

      <Button variant="text" className={classes.item}>
        <Link to={'/sugesstions'}>
          <Typography variant={'subtitle'} className={classes.text}>
            Improve
          </Typography>
        </Link>
      </Button>

      <Button variant="text" className={classes.item}>
        <Typography variant={'subtitle'} className={classes.text}>
          Report Error
        </Typography>
      </Button>
    </div>
  );
});
