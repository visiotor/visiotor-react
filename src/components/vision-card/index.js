import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import { Share, Favorite, Report, Delete, Edit } from '@material-ui/icons';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import { Link } from 'react-router-dom';
import { getLikedStatus, setLike, deleteLike, deleteVision } from '../../utils/api';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { SERVER_IP_OR_ADDRESS, API_VERSION } from '../../utils/api';

const useStyles = makeStyles({
  card: {
    maxWidth: '100%',
    margin: 2,
  },
  media: {
    height: 250,
  },
  description: {
    width: 600,
  },
});

export default function MediaCard(props) {
  const classes = useStyles();
  const [liked, setLiked] = useState(false);
  const [open, setOpen] = React.useState(false);
  const [openReport, setOpenReport] = React.useState(false);

  const [likeCount, setLikeCount] = useState(0);
  const [init, setInit] = useState(false);
  useEffect(() => {
    if (!init) {
      getLikedStatus(props._id).then(({ data, status }) => {
        if (status === 200) {
          setLiked(data.payload.liked);
          setLikeCount(data.payload.likeCount);
          setInit(true);
        } else {
          //todo: show error!
        }
      });
    }
  });
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDeleteVision = () => {
    handleClose();
    deleteVision(props._id).then(({ data, status }) => {
      if (status === 200) {
        props.refresh();
      }
    });
  };

  const handleEditClick = () => {};

  function handleLike() {
    if (liked) {
      deleteLike(props._id).then(({ data, status }) => {
        if (status === 200 && data.success) {
          setLiked(false);
          setLikeCount(data.payload.likeCount);
        }
      });
    } else {
      setLike(props._id).then(({ data, status }) => {
        if (status === 200 && data.success) {
          setLiked(true);
          setLikeCount(data.payload.likeCount);
        }
      });
    }
  }

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Link to={`/_u/@${props.owner.username}`}>
            <Avatar aria-label="Recipe" className={classes.avatar}>
              {props.owner ? props.owner.fullname[0] : 'A'}
            </Avatar>
          </Link>
        }
        action={
          <Tooltip title={'Report'}>
            <IconButton aria-label="report" onClick={() => setOpenReport(true)}>
              <Report />
            </IconButton>
          </Tooltip>
        }
        title={props.owner ? props.owner.fullname : 'Owner'}
        subheader={new Date(props.createdAt).toLocaleDateString()}
      />
      <Link to={`/_v/${props._id}`}>
        <CardActionArea>
          <CardMedia
            className={classes.media}
            image={`${SERVER_IP_OR_ADDRESS}/${API_VERSION}/vision/${props._id}/cover`}
          />
          <CardContent>
            <Typography color={'textPrimary'} gutterBottom variant="h5" component="h2">
              {props.title}
            </Typography>
            <Typography
              variant="body2"
              color="textPrimary"
              component="p"
              className={classes.description}
            >
              {props.description}
            </Typography>
          </CardContent>
        </CardActionArea>
      </Link>
      <CardActions>
        <Tooltip title={'Like'}>
          <IconButton size="small" color={liked ? 'primary' : 'default'} onClick={handleLike}>
            {likeCount}
            <Favorite />
          </IconButton>
        </Tooltip>
        <Tooltip title={'share'}>
          <IconButton size="small" color="default">
            <Share />
          </IconButton>
        </Tooltip>
        {localStorage.username === props.owner.username ? (
          <Tooltip title={'Delete Vision'}>
            <IconButton onClick={handleClickOpen}>
              <Delete />
            </IconButton>
          </Tooltip>
        ) : null}
        {localStorage.username === props.owner.username ? (
          <Tooltip title={'Edit Vision'}>
            <IconButton onClick={handleEditClick}>
              <Edit />
            </IconButton>
          </Tooltip>
        ) : null}
        {props.category
          ? props.category.map(c => (
              <Link to={`/_c/${c}`} key={c}>
                <Chip label={c} color={'secondary'} variant={'outlined'} clickable />
              </Link>
            ))
          : null}
      </CardActions>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{'Delete Vision!'}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Are you sure to delete this vision?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            No
          </Button>
          <Button onClick={handleDeleteVision} color="default">
            Yes
          </Button>
        </DialogActions>
      </Dialog>

      <Dialog open={openReport} onClose={() => setOpenReport(false)}>
        <DialogTitle>Report!</DialogTitle>
        <DialogContent>
          <Typography>Please describe how this vision breaks the rules...</Typography>
          <TextField variant="outlined" fullWidth label={'Description...'} />
        </DialogContent>
        <DialogActions>
          <Button fullWidth>Report!</Button>
        </DialogActions>
      </Dialog>
    </Card>
  );
}
